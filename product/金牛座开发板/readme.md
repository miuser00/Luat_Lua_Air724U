### 此处所有的demo不能在iCool手机上使用，只能在金牛座开发板上验证调试



# 一、widgets

​	此目录下存放基本控件demo：

#### 	1.1、single

​				单个控件的功能演示；例如演示一个button

#### 	1.2、combination

​				多种控件组合的功能演示；例如演示一个button、一个label、一个slider三种控件的功能组合

# 二、liteApp

​	此目录下存放轻量级应用demo

#### 	2.1、printer

​				LVGL官网打印机demo

#### 	2.2、game

​				一些小游戏



# 三、project

此目录下存放项目参考方案，例如“基于iRTU设计的农业控制大棚系统”参考方案
####    mediaAirController
            空调-多媒体控制器：基于LVGL编写，包含空调控制器，媒体下载，天气预报，远程升级等功能展示
<div align="center">
<img src="https://gitee.com/openLuat/Luat_Lua_Air724U/raw/beta/product/%E9%87%91%E7%89%9B%E5%BA%A7%E5%BC%80%E5%8F%91%E6%9D%BF/3.project/mediaAirController/%E6%95%88%E6%9E%9C%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211207103024.jpg" width="60%">
<img src="https://gitee.com/openLuat/Luat_Lua_Air724U/raw/beta/product/%E9%87%91%E7%89%9B%E5%BA%A7%E5%BC%80%E5%8F%91%E6%9D%BF/3.project/mediaAirController/%E6%95%88%E6%9E%9C%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211207103011.jpg" width="60%">
<img src="https://gitee.com/openLuat/Luat_Lua_Air724U/raw/beta/product/%E9%87%91%E7%89%9B%E5%BA%A7%E5%BC%80%E5%8F%91%E6%9D%BF/3.project/mediaAirController/%E6%95%88%E6%9E%9C%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211207103021.jpg" width="60%">
<img src="https://gitee.com/openLuat/Luat_Lua_Air724U/raw/beta/product/%E9%87%91%E7%89%9B%E5%BA%A7%E5%BC%80%E5%8F%91%E6%9D%BF/3.project/mediaAirController/%E6%95%88%E6%9E%9C%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20211207103000.jpg" width="60%">
</div>




