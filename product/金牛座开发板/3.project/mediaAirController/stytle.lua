module(..., package.seeall)

    stytle_statusBox = lvgl.style_t()
    lvgl.style_init(stytle_statusBox)
    lvgl.style_set_bg_color(stytle_statusBox, lvgl.CONT_PART_MAIN,lvgl.color_make(0x0f, 0x3e, 0x74))
    lvgl.style_set_radius(stytle_statusBox, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(stytle_statusBox, lvgl.CONT_PART_MAIN, 0)
    --透明
    -- lvgl.style_set_bg_opa(stytle_statusBox, lvgl.CONT_PART_MAIN,0)
------------------------------------------------------------------------------------------------------
    stytle_contentBox = lvgl.style_t()
    lvgl.style_init(stytle_contentBox)
    lvgl.style_set_bg_color(stytle_contentBox, lvgl.CONT_PART_MAIN,lvgl.color_make(0x00, 0x0d, 0x3a))
    lvgl.style_set_radius(stytle_contentBox, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(stytle_contentBox, lvgl.CONT_PART_MAIN, 0)
    --透明
    -- lvgl.style_set_bg_opa(stytle_contentBox, lvgl.CONT_PART_MAIN,0)


    stytle_divBox = lvgl.style_t()
    lvgl.style_init(stytle_divBox)
    lvgl.style_set_bg_opa(stytle_divBox, lvgl.CONT_PART_MAIN,0)
    lvgl.style_set_radius(stytle_divBox, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_border_width(stytle_divBox, lvgl.CONT_PART_MAIN, 0)


    stytle_icon = lvgl.style_t()
    lvgl.style_init(stytle_icon)
    lvgl.style_set_margin_left(stytle_icon, lvgl.CONT_PART_MAIN,35)
	lvgl.style_set_margin_right(stytle_icon, lvgl.CONT_PART_MAIN,35)

    style_activty_airCtr_btn=lvgl.style_t()
    lvgl.style_init(style_activty_airCtr_btn)
    lvgl.style_set_bg_color(style_activty_airCtr_btn, lvgl.CONT_PART_MAIN, lvgl.color_make(0x01, 0xa2, 0xb1))
    lvgl.style_set_border_width(style_activty_airCtr_btn, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_radius(style_activty_airCtr_btn, lvgl.CONT_PART_MAIN, 5)

    style_activty_media_bar=lvgl.style_t()
    lvgl.style_init(style_activty_media_bar)
    lvgl.style_set_border_width(style_activty_media_bar, lvgl.CONT_PART_MAIN, 0)
    lvgl.style_set_radius(style_activty_media_bar, lvgl.CONT_PART_MAIN, 0)
