module(..., package.seeall)

require "utils"
-- sys.taskInit(function()
-- while true do
--     url="https://cdn.openluat-luatcommunity.openluat.com/images/20211125101844996_testimg.png"
--     a=url:split('/')
--     print(a,#a,a[#a])
--     sys.wait(1000)
-- end

-- end)

--------------
-- sProcessedLen=0
-- FileName="123.png"
-- local function process(stepData,totalLen,statusCode)
--     if stepData and totalLen then
--         if statusCode=="200" or statusCode=="206" then
--             sProcessedLen = sProcessedLen + stepData:len()
--             local c = io.writeFile(fileName,stepData,"a+b")
--             print("写入：",c)
--             log.info("download:",(sProcessedLen*100/totalLen).."%")
--         end
--     end
-- end

-- http.request("GET","https://cdn.openluat-luatcommunity.openluat.com/images/20211125101844996_testimg.png",nil,nil,nil,30000,nil,process)

local downLoadTab={}

function fileInTable(filename)
    for k, v in pairs(downLoadTab) do
        if filename ==v then return true end
    end
    return false
end

function removeList(tb,filename)
	for i = #tb, 1, -1 do
	    if tb[i] == filename then
	        table.remove(tb, i)
	    end
	end
end

function downloadService(url,fnc)
    local sProcessedLen=0
    local t=url:split('/')
    local fileName=t[#t]
    if io.exists(fileName) then os.remove(fileName) end
    local tempFileName="temp_"..fileName--保存为临时下载文件
    if io.exists(tempFileName) then os.remove(tempFileName) end
    local function process(stepData,totalLen,statusCode)
        if stepData and totalLen then
            if statusCode=="200" or statusCode=="206" then
                sProcessedLen = sProcessedLen + stepData:len()
                local c = io.writeFile(tempFileName,stepData,"a+b")
                -- print("写入：",c)
                -- log.info("download:"..fileName,(sProcessedLen*100/totalLen).."%")
                if (sProcessedLen*100/totalLen)==100 then
                    os.rename(tempFileName,fileName)
                    removeList(downLoadTab,fileName)
                end
                fnc(sProcessedLen*100/totalLen,fileName)
            else
                removeList(downLoadTab,fileName)
                -- table.insert(tbl, 3, 666)
            end
        end
    end
    table.insert(downLoadTab,fileName)
    http.request("GET",url,nil,nil,nil,30000,nil,process)
end