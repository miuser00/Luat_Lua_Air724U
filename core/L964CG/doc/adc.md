@[TOC]

# adc
adc数模转换模块
## adc.open(id,scale)	

打开ADC通道

**参数**

|参数|类型|释义|取值|
|-|-|-|-|
|id|number|需要打开的adc的通道id|0,1|
|scale|number|设置adc检测的范围|adc.SCALE_1V250<br>adc.SCALE_2V444<br>adc.SCALE_3V233<br>adc.SCALE_5V000|

**返回值**

|返回值|类型|释义|取值|
|-|-|-|-|
|result|number|返回ADC打开的结果|0打开失败1打开成功|

**例子**

```lua
-- 打开ADC通道2
local id=2  
local result=adc.open(id)  
if result==1 then  
    log.info("adc",id,"打开成功！");  
end

```

---
# 其他接口见:
https://doc.openluat.com/wiki/21?wiki_page_id=2242




